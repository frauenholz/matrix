/** Dependencies */
import {
    Collection,
    Forms,
    Slots,
    alias,
    definition,
    editor,
    insertVariable,
    isString,
    name,
    pgettext,
    score,
} from "tripetto";
import { Matrix } from "./";

export class Column extends Collection.Item<Matrix> {
    @definition("string")
    @name
    label = "";

    @definition("string", "optional")
    @alias
    value?: string;

    @definition("number", "optional")
    @score
    score?: number;

    @editor
    defineEditor(): void {
        this.editor.option({
            name: pgettext("block:matrix", "Column"),
            form: {
                title: pgettext("block:matrix", "Column name"),
                controls: [
                    new Forms.Text(
                        "singleline",
                        Forms.Text.bind(this, "label", "")
                    )
                        .action("@", insertVariable(this))
                        .autoFocus()
                        .autoSelect()
                        .enter(this.editor.close)
                        .escape(this.editor.close),
                ],
            },
            locked: true,
        });

        this.editor.group(pgettext("block:matrix", "Options"));
        this.editor.option({
            name: pgettext("block:matrix", "Identifier"),
            form: {
                title: pgettext("block:matrix", "Column identifier"),
                controls: [
                    new Forms.Text(
                        "singleline",
                        Forms.Text.bind(this, "value", undefined)
                    ),
                    new Forms.Static(
                        pgettext(
                            "block:matrix",
                            "If a column identifier is set, this identifier will be used instead of the label."
                        )
                    ),
                ],
            },
            activated: isString(this.value),
        });

        const scoreSlot = this.ref.slots.select<Slots.Numeric>(
            "score",
            "feature"
        );

        this.editor.option({
            name: pgettext("block:matrix", "Score"),
            form: {
                title: pgettext("block:matrix", "Score"),
                controls: [
                    new Forms.Numeric(
                        Forms.Numeric.bind(this, "score", undefined)
                    )
                        .precision(scoreSlot?.precision || 0)
                        .digits(scoreSlot?.digits || 0)
                        .decimalSign(scoreSlot?.decimal || "")
                        .thousands(
                            scoreSlot?.separator ? true : false,
                            scoreSlot?.separator || ""
                        )
                        .prefix(scoreSlot?.prefix || "")
                        .prefixPlural(scoreSlot?.prefixPlural || undefined)
                        .suffix(scoreSlot?.suffix || "")
                        .suffixPlural(scoreSlot?.suffixPlural || undefined),
                ],
            },
            activated: true,
            locked: scoreSlot ? true : false,
            disabled: scoreSlot ? false : true,
        });
    }
}
