/** Package information retrieved from `package.json` using webpack. */
declare const PACKAGE_NAME: string;
declare const PACKAGE_VERSION: string;

/** Dependencies */
import {
    Collection,
    Markdown,
    NodeBlock,
    affects,
    conditions,
    definition,
    each,
    editor,
    isBoolean,
    npgettext,
    pgettext,
    supplies,
    tripetto,
} from "tripetto";
import { Row } from "./row";
import { Column } from "./column";
import { MatrixCondition } from "./conditions/matrix";
import { ScoreCondition } from "./conditions/score";
import { TScoreModes } from "../runner/conditions/score";

/** Assets */
import ICON from "../../assets/icon.svg";
import ICON_SCORE from "../../assets/score.svg";

@tripetto({
    type: "node",
    identifier: PACKAGE_NAME,
    version: PACKAGE_VERSION,
    icon: ICON,
    get label() {
        return pgettext("block:matrix", "Matrix");
    },
})
export class Matrix extends NodeBlock {
    @definition("columns")
    @supplies<Matrix>("#collection", "rows")
    readonly columns = Collection.of<Column, Matrix>(Column, this);

    @definition("items")
    @affects("#name")
    readonly rows = Collection.of<Row, Matrix>(Row, this);

    @definition("boolean", "optional")
    @affects("#required")
    required?: boolean;

    @definition("boolean", "optional")
    @affects("#slots")
    @affects("#collection", "rows")
    exportable?: boolean;

    get label() {
        return npgettext(
            "block:matrix",
            "%2 (%1 row)",
            "%2 (%1 rows)",
            this.rows.count,
            this.type.label
        );
    }

    @editor
    defineEditor(): void {
        this.editor.name();
        this.editor.description();
        this.editor.explanation();

        const rows = this.editor.collection({
            collection: this.rows,
            title: pgettext("block:matrix", "Rows"),
            placeholder: pgettext("block:matrix", "Unnamed row"),
            sorting: "manual",
            autoOpen: true,
            allowVariables: true,
            allowImport: true,
            allowExport: true,
            allowDedupe: true,
            showAliases: true,
            markdown:
                Markdown.MarkdownFeatures.Formatting |
                Markdown.MarkdownFeatures.Hyperlinks,
            indicator: (row) =>
                (!isBoolean(this.required) &&
                    this.slots.select(row.id)?.required &&
                    pgettext("block:matrix", "Required").toUpperCase()) ||
                undefined,
            emptyMessage: pgettext(
                "block:matrix",
                "Click the + button to add a row..."
            ),
        });

        const columns = this.editor.collection({
            collection: this.columns,
            title: pgettext("block:matrix", "Columns"),
            placeholder: pgettext("block:matrix", "Unnamed column"),
            autoOpen: true,
            showAliases: true,
            allowVariables: true,
            allowImport: true,
            allowExport: true,
            allowDedupe: true,
            markdown:
                Markdown.MarkdownFeatures.Formatting |
                Markdown.MarkdownFeatures.Hyperlinks,
            sorting: "manual",
            emptyMessage: pgettext(
                "block:matrix",
                "Click the + button to add a column..."
            ),
        });

        this.editor.groups.options();
        this.editor.required(
            this,
            pgettext("block:matrix", "All rows need an answer"),
            (required) => {
                this.rows.each((row) => {
                    row.defineSlot().required = required;

                    row.refresh();
                });

                rows.refresh();
            }
        );

        this.editor.scores({
            target: this,
            collection: columns,
            description: pgettext(
                "block:matrix",
                "Generates a score based on the selected column for each row. Open the settings panel for each column to set the score."
            ),
        });

        this.editor.visibility();
        this.editor.exportable(this);
    }

    @conditions
    defineConditions(): void {
        this.rows.each((row: Row) => {
            if (row.name) {
                const group = this.conditions.group(row.name, undefined, true);

                this.columns.each((column: Column) => {
                    if (column.label) {
                        group.template({
                            condition: MatrixCondition,
                            label: column.label,
                            burst: true,
                            props: {
                                slot: this.slots.select(row.id),
                                row: row,
                                column: column,
                            },
                        });
                    }
                });
            }
        });

        const score = this.slots.select("score", "feature");

        if (score && score.label) {
            const group = this.conditions.group(
                score.label,
                ICON_SCORE,
                false,
                true
            );

            each(
                [
                    {
                        mode: "equal",
                        label: pgettext("block:matrix", "Score is equal to"),
                    },
                    {
                        mode: "not-equal",
                        label: pgettext(
                            "block:matrix",
                            "Score is not equal to"
                        ),
                    },
                    {
                        mode: "below",
                        label: pgettext("block:matrix", "Score is lower than"),
                    },
                    {
                        mode: "above",
                        label: pgettext("block:matrix", "Score is higher than"),
                    },
                    {
                        mode: "between",
                        label: pgettext("block:matrix", "Score is between"),
                    },
                    {
                        mode: "not-between",
                        label: pgettext("block:matrix", "Score is not between"),
                    },
                    {
                        mode: "defined",
                        label: pgettext("block:matrix", "Score is calculated"),
                    },
                    {
                        mode: "undefined",
                        label: pgettext(
                            "block:matrix",
                            "Score is not calculated"
                        ),
                    },
                ],
                (condition: { mode: TScoreModes; label: string }) => {
                    group.template({
                        condition: ScoreCondition,
                        label: condition.label,
                        autoOpen:
                            condition.mode !== "defined" &&
                            condition.mode !== "undefined",
                        props: {
                            slot: score,
                            mode: condition.mode,
                            value: 0,
                            to:
                                condition.mode === "between" ||
                                condition.mode === "not-between"
                                    ? 0
                                    : undefined,
                        },
                    });
                }
            );
        }
    }
}
