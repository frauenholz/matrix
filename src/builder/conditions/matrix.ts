/** Package information retrieved from `package.json` using webpack. */
declare const PACKAGE_NAME: string;
declare const PACKAGE_VERSION: string;

/** Dependencies */
import {
    ConditionBlock,
    Forms,
    affects,
    collection,
    editor,
    makeMarkdownSafe,
    markdownifyToString,
    pgettext,
    tripetto,
} from "tripetto";
import { Matrix } from "..";
import { Row } from "../row";
import { Column } from "../column";

/** Assets */
import ICON from "../../../assets/icon.svg";

@tripetto({
    type: "condition",
    context: PACKAGE_NAME,
    identifier: PACKAGE_NAME,
    version: PACKAGE_VERSION,
    icon: ICON,
    get label() {
        return pgettext("block:matrix", "Matrix");
    },
})
export class MatrixCondition extends ConditionBlock {
    private columnRef: Column | undefined;
    readonly allowMarkdown = true;

    @affects("#name")
    @collection("#rows")
    row: Row | undefined;

    @affects("#name")
    @collection("#columns")
    get column() {
        return this.columnRef;
    }

    set column(column: Column | undefined) {
        if (this.columnRef) {
            this.columnRef.unhookContext(this);
        }

        this.columnRef = column;

        if (column) {
            column.hook("OnRename", "framed", () => this.rerender(), this);
            column.hook("OnDelete", "framed", () => this.delete(), this);
        }
    }

    get name() {
        return (
            (this.row
                ? markdownifyToString(this.row.name) +
                  (this.column
                      ? ` \`${makeMarkdownSafe(this.column.label)}\``
                      : "")
                : "") || this.type.label
        );
    }

    get title() {
        return markdownifyToString(this.row?.name || "");
    }

    @editor
    defineEditor(): void {
        const columns: Forms.IDropdownOption<Column>[] = [];

        if (this.node && this.node.block instanceof Matrix) {
            this.node.block.columns.each((column: Column) => {
                if (column.label || column === this.column) {
                    columns.push({
                        label: column.label,
                        value: column,
                    });
                }
            });
        }

        this.editor.form({
            controls: [
                new Forms.Dropdown(
                    columns,
                    Forms.Dropdown.bind(this, "column", undefined)
                ),
            ],
        });
    }
}
