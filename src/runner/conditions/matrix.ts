/** Package information retrieved from `package.json` using webpack. */
declare const PACKAGE_NAME: string;

/** Dependencies */
import {
    ConditionBlock,
    condition,
    tripetto,
} from "tripetto-runner-foundation";

@tripetto({
    type: "condition",
    identifier: PACKAGE_NAME,
})
export class MatrixCondition extends ConditionBlock<{
    readonly row: string | undefined;
    readonly column: string | undefined;
}> {
    @condition
    isSelected(): boolean {
        const rowSlot = this.valueOf<string>();

        return (rowSlot && rowSlot.reference === this.props.column) || false;
    }
}
