/** Dependencies */
import {
    MarkdownFeatures,
    NodeBlock,
    Slots,
    Value,
    findFirst,
    markdownifyToString,
    reduce,
    validator,
} from "tripetto-runner-foundation";
import { IMatrixRow } from "./row";
import { IMatrixColumn } from "./column";

export abstract class Matrix extends NodeBlock<{
    readonly columns?: IMatrixColumn[];
    readonly rows?: IMatrixRow[];
    readonly required?: boolean;
}> {
    /** Contains the score slot. */
    readonly scoreSlot = this.valueOf<number, Slots.Numeric>(
        "score",
        "feature"
    );

    /** Contains if the matrix is required. */
    readonly required =
        this.props.required ||
        findFirst(this.props.rows, (row: IMatrixRow) => this.isRowRequired(row))
            ? true
            : false;

    /** Retrieves a matrix row slot. */
    rowSlot(row: IMatrixRow): Value<string, Slots.String> | undefined {
        return this.valueOf<string>(row.id, "dynamic", {
            confirm: true,
            modifier: (data) => {
                if (data.value) {
                    if (!data.reference) {
                        const selected =
                            findFirst(
                                this.props.columns,
                                (column) => column.value === data.value
                            ) ||
                            findFirst(
                                this.props.columns,
                                (column) => column.id === data.value
                            ) ||
                            findFirst(
                                this.props.columns,
                                (column) => column.label === data.value
                            ) ||
                            findFirst(
                                this.props.columns,
                                (column) =>
                                    column.label.toLowerCase() ===
                                    data.value.toLowerCase()
                            );

                        return {
                            value:
                                selected && (selected.value || selected.label),
                            reference: selected && selected.id,
                        };
                    } else if (
                        !findFirst(
                            this.props.columns,
                            (column) => column.id === data.reference
                        )
                    ) {
                        return {
                            value: undefined,
                            reference: undefined,
                        };
                    }
                }
            },
            onChange: () => {
                if (this.scoreSlot) {
                    this.scoreSlot.set(
                        reduce(
                            this.props.rows,
                            (score, r) =>
                                score +
                                (findFirst(
                                    this.props.columns,
                                    (column) =>
                                        column.id ===
                                        this.valueOf(r.id)?.reference
                                )?.score || 0),
                            0
                        )
                    );
                }
            },
        });
    }

    /** Verifies if a row is required. */
    isRowRequired(row: IMatrixRow): boolean {
        return this.rowSlot(row)?.slot.required || false;
    }

    /** Verifies if a column is selected. */
    isColumnSelected(row: IMatrixRow, column: IMatrixColumn): boolean {
        return this.rowSlot(row)?.reference === column.id;
    }

    /** Retrieves the selected column for a row. */
    getSelectedColumn(row: IMatrixRow): IMatrixColumn | undefined {
        return findFirst(this.props.columns, (column: IMatrixColumn) =>
            this.isColumnSelected(row, column)
        );
    }

    /** Sets the selected column for a row. */
    selectColumn(row: IMatrixRow, column: IMatrixColumn | undefined): void {
        this.rowSlot(row)?.set(
            column && (column.value || column.label),
            column && column.id
        );
    }

    /** Retrieves the columns. */
    columns<T>(props: {
        readonly markdownifyToJSX: (md: string, lineBreaks?: boolean) => T;
    }): (Omit<IMatrixColumn, "label"> & {
        readonly name: string;
        readonly label: T;
    })[] {
        return (
            this.props.columns?.map((column) => ({
                ...column,
                name: markdownifyToString(
                    column.label,
                    this.context,
                    undefined,
                    false,
                    MarkdownFeatures.Formatting | MarkdownFeatures.Hyperlinks
                ),
                label: props.markdownifyToJSX(column.label, false),
            })) || []
        );
    }

    /** Retrieves the rows. */
    rows<T>(props: {
        readonly tabIndex?: number;
        readonly markdownifyToJSX: (md: string, lineBreaks?: boolean) => T;
    }): (Omit<IMatrixRow, "explanation"> & {
        readonly label: T;
        readonly explanation?: T;
        readonly value?: Value<string, Slots.String>;
        readonly required: boolean;
        readonly tabIndex?: number;
    })[] {
        return (
            this.props.rows?.map((row) => {
                const value = this.valueOf<string>(row.id);
                const required =
                    !this.props.required && value && value.slot.required
                        ? true
                        : false;

                return {
                    ...row,
                    label: props.markdownifyToJSX(row.name, false),
                    explanation:
                        (row.explanation &&
                            props.markdownifyToJSX(row.explanation, true)) ||
                        undefined,
                    required,
                    value,
                    tabIndex: props.tabIndex,
                };
            }) || []
        );
    }

    @validator
    validate(): boolean {
        return findFirst(this.props.rows, (row: IMatrixRow) => {
            const rowSlot = this.rowSlot(row);

            return (this.props.required || rowSlot?.slot.required) &&
                !rowSlot?.reference
                ? true
                : false;
        })
            ? false
            : true;
    }
}
