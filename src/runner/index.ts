/** Imports */
import "./conditions/matrix";
import "./conditions/score";

/** Exports */
export { Matrix } from "./matrix";
export { IMatrixRow } from "./row";
export { IMatrixColumn } from "./column";
