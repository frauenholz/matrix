export interface IMatrixRow {
    /** Id of the row. */
    readonly id: string;

    /** Name of the row. */
    readonly name: string;

    /** Alias of the row. */
    readonly alias?: string;

    /** Contains the explanation for a row. */
    readonly explanation?: string;
}
